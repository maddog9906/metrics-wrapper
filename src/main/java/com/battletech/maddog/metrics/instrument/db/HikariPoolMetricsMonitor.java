package com.battletech.maddog.metrics.instrument.db;

import com.battletech.maddog.metrics.instrument.MetricsMonitor;
import com.zaxxer.hikari.pool.HikariPool;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;

import java.util.Optional;

public class HikariPoolMetricsMonitor implements MetricsMonitor<HikariPool> {

    private static final String METRIC_PREFIX = "db-pool";

    @Override
    public HikariPool addInstrumentation(MeterRegistry registry, HikariPool pool, String name) {

        String prefix = METRIC_PREFIX + "." + Optional.ofNullable(name).orElse("hikari")  + ".";

        Gauge.builder(prefix + "max", pool, a-> a.config.getMaximumPoolSize())
                .register(registry);

        Gauge.builder(prefix + "active", pool, HikariPool::getActiveConnections)
                .register(registry);

        Gauge.builder(prefix + "idle", pool, HikariPool::getIdleConnections)
                .register(registry);

        Gauge.builder(prefix + "waiting", pool, HikariPool::getThreadsAwaitingConnection)
                .register(registry);

        Gauge.builder(prefix + "utilisation", pool, a -> (double) a.getTotalConnections()/ a.config.getMaximumPoolSize())
                .register(registry);

        return null;
    }
}
