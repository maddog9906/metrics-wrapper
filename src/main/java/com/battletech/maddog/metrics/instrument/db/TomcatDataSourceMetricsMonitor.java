package com.battletech.maddog.metrics.instrument.db;

import com.battletech.maddog.metrics.instrument.MetricsMonitor;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import org.apache.tomcat.jdbc.pool.DataSource;

import java.util.Optional;

public class TomcatDataSourceMetricsMonitor implements MetricsMonitor<DataSource> {

    private static final String METRIC_PREFIX = "datasource";

    @Override
    public DataSource addInstrumentation(MeterRegistry registry, DataSource ds, String name) {

        String prefix = METRIC_PREFIX + "." + Optional.ofNullable(name).orElse("tomcat") + ".";

        Gauge.builder(prefix + "max", ds, DataSource::getMaxActive)
            .register(registry);

        Gauge.builder(prefix + "active", ds, DataSource::getActive)
                .register(registry);

        Gauge.builder(prefix + "idle", ds, DataSource::getIdle)
                .register(registry);

        Gauge.builder(prefix + "created", ds, DataSource::getCreatedCount)
                .register(registry);

        Gauge.builder(prefix + "waiting", ds, DataSource::getWaitCount)
                .register(registry);

        Gauge.builder(prefix + "utilisation", ds, a -> (double) (a.getActive() + a.getIdle()) / a.getMaxActive())
                .register(registry);

        return ds;


    }
}
