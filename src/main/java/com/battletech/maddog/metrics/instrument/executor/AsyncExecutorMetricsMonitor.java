package com.battletech.maddog.metrics.instrument.executor;

import com.battletech.maddog.metrics.instrument.MetricsMonitor;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

class AsyncExecutorMetricsMonitor<T extends ThreadPoolTaskExecutor> implements MetricsMonitor<T> {

    private static final String METRIC_PREFIX = "task-executor";

    @Override
    public T addInstrumentation(MeterRegistry registry, T executor, String name) {
        String prefix = METRIC_PREFIX + "." + name + ".";

        Gauge.builder(prefix + "max-threads", executor, ThreadPoolTaskExecutor::getMaxPoolSize)
                .register(registry);

        Gauge.builder(prefix + "available-threads", executor, ThreadPoolTaskExecutor::getPoolSize)
                .register(registry);

        Gauge.builder(prefix + "active-threads", executor, ThreadPoolTaskExecutor::getActiveCount)
                .register(registry);

        Gauge.builder(prefix + "thread-utilisation", executor, a -> (double) (a.getActiveCount() / a.getPoolSize()))
                .register(registry);

        return executor;
    }
}
