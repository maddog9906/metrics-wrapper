package com.battletech.maddog.metrics.instrument;

import io.micrometer.core.instrument.MeterRegistry;

public interface MetricsMonitor<T> {

    T addInstrumentation(MeterRegistry registry, T target, String name);
}
