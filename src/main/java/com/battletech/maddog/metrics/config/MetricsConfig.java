package com.battletech.maddog.metrics.config;

import com.battletech.maddog.metrics.service.InstrumentedAspect;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@Configuration
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class MetricsConfig {

    private final MeterRegistry meterRegistry;

    @Autowired
    public MetricsConfig(MeterRegistry meterRegistry) {
        this.meterRegistry = meterRegistry;
    }

    @Bean
    public InstrumentedAspect instrumentedAspect() {
        return new InstrumentedAspect(meterRegistry);
    }

}
