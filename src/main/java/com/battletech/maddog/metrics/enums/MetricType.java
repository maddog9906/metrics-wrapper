package com.battletech.maddog.metrics.enums;

public enum MetricType {
    Counter("counter"),
    Timer("timer"),
    Gauge("gauge"),
    Histogram("histogram");

    MetricType(String prefix) {
        this.prefix = prefix;
    }

    String prefix;

    public String getPrefix() {
        return prefix;
    }
}

