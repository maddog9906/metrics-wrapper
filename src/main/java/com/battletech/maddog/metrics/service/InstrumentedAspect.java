package com.battletech.maddog.metrics.service;

import com.battletech.maddog.metrics.annotation.*;
import com.battletech.maddog.metrics.enums.MetricType;
import com.google.common.base.CaseFormat;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Aspect
public class InstrumentedAspect {

    private Map<String, Double> gaugeCache = new HashMap<>();

    private final MeterRegistry registry;

    public InstrumentedAspect(MeterRegistry registry) {
        this.registry = registry;
    }

    @Around("@annotation(com.battletech.maddog.metrics.annotation.Timed)")
    public Object injectTimedMetric(ProceedingJoinPoint joinPoint) throws Throwable {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Timed theAnnotation = method.getAnnotation(Timed.class);
        Object result;
        long startTime = System.currentTimeMillis();
        try {
            result = joinPoint.proceed();
        } catch (Throwable e){
            throw e;
        } finally {
            Timer timer = registry.timer(toMetricName(theAnnotation.name(), MetricType.Timer), theAnnotation.extraTags());
            timer.record(System.currentTimeMillis() -  startTime, TimeUnit.MILLISECONDS);
        }
        return result;
    }

    @AfterReturning(
            pointcut = "@annotation(com.battletech.maddog.metrics.annotation.Gauged)",
            returning= "result")
    public void injectGaugeMetric(JoinPoint joinPoint, Object result) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Gauged theAnnotation = method.getAnnotation(Gauged.class);

        if (Number.class.isAssignableFrom(result.getClass())) {
            String metricName = toMetricName(theAnnotation.name(), MetricType.Gauge);
            gaugeCache.put(metricName, ((Number)result).doubleValue());
            Gauge.builder(metricName, ()-> gaugeCache.get(metricName))
                    .tags(theAnnotation.extraTags())
                    .register(registry);
        }
    }

    @Before("@annotation(com.battletech.maddog.metrics.annotation.Counted)")
    public void injectMeterMetric(JoinPoint joinPoint){
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        Counted theCounted = method.getAnnotation(Counted.class);
        if (theCounted != null) {
            Counter counter = Counter
                                .builder(toMetricName(theCounted.name(), MetricType.Counter))
                                .tags(theCounted.extraTags())
                                .register(registry);
            counter.increment();
        }
    }

    @AfterThrowing(
            pointcut = "@annotation(com.battletech.maddog.metrics.annotation.ExceptionCounted)",
            throwing = "t")
    public void injectExceptionMeterMetric(JoinPoint joinPoint, Throwable t) {
        MethodSignature signature = (MethodSignature) joinPoint.getSignature();
        Method method = signature.getMethod();
        ExceptionCounted theAnnotation = method.getAnnotation(ExceptionCounted.class);

        if (theAnnotation.cause().isAssignableFrom(t.getClass())){
            Counter counter = Counter
                    .builder(toMetricName(theAnnotation.name() + ".exceptions", MetricType.Counter))
                    .tags(theAnnotation.extraTags())
                    .register(registry);
            counter.increment();
        }
    }

    private static String toMetricName(String name, MetricType metricType){
        if (name.startsWith(metricType.getPrefix() + "."))
            return toSnakeCase(name);
        else{
            return toSnakeCase(metricType.getPrefix() + "." + name);
        }
    }

    private static String toSnakeCase(String s) {
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_HYPHEN, s);
    }
}
