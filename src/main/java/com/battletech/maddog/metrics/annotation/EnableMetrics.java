package com.battletech.maddog.metrics.annotation;

import com.battletech.maddog.metrics.config.MetricsConfig;
import org.springframework.context.annotation.Import;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Import({MetricsConfig.class})
public @interface EnableMetrics {
}
