package com.battletech.maddog.metrics.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ExceptionCounted {
    String name() default "";
    String[] extraTags() default {};
    Class<? extends Throwable> cause() default Exception.class;
}
