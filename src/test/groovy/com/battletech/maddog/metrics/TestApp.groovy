package com.battletech.maddog.metrics

import com.battletech.maddog.metrics.annotation.EnableMetrics
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@EnableMetrics
@SpringBootApplication
class TestApp {

    static void main(String[] args) {
        SpringApplication.run(TestApp.class, args)
    }
}
