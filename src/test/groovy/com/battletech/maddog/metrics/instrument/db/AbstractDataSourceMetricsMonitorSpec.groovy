package com.battletech.maddog.metrics.instrument.db

import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.simple.SimpleMeterRegistry
import org.hsqldb.Server
import spock.lang.Specification

abstract class AbstractDataSourceMetricsMonitorSpec extends Specification {

    MeterRegistry registry
    Server dbServer

    def setup(){
        registry = new SimpleMeterRegistry()
        dbServer = new Server()

        dbServer.setDatabaseName(0, 'instrument_test')
        dbServer.setDatabasePath(0, './build/test-db/instrument_test')
        dbServer.start()
    }

    def cleanup() {
        dbServer.stop()
    }
}
