package com.battletech.maddog.metrics.instrument.db

import com.battletech.maddog.metrics.helper.DbHelper
import groovyx.gpars.GParsPool

class TomcatDataSourceMetricsMonitorSpec extends AbstractDataSourceMetricsMonitorSpec {

    def 'exposes connection pool and query metrics'() {
        given:
        def ds = DbHelper.getTomcatDataSource([
                    dbURL                           : 'jdbc:hsqldb:hsql://localhost/instrument_test',
                    dbUser                          : 'SA',
                    dbPwd                           : '',
                    maxActive                       : 20,
                    maxAge                          : 600000,
                    maxWait                         : 10000,
                    maxIdle                         : 15,
                    minIdle                         : 2,
                    initialSize                     : 1,
                    validationQuery                 : 'SELECT 1 FROM information_schema.system_users',
                    minEvictableIdleTimeMillis      : 100,
                    timeBetweenEvictionRunsMillis   : 1000


                ])
        def metricsMonitor = new TomcatDataSourceMetricsMonitor()
        ds = metricsMonitor.addInstrumentation(registry, ds, null)

        List taskList = 1..100

        when:
        2.times {
            GParsPool.withPool(50) {
                taskList.eachParallel {
                    def con = ds.getConnection()
                    def preparedStatement = con.prepareStatement('SELECT 1 FROM INFORMATION_SCHEMA.SYSTEM_USERS')
                    def preparedResultSet = preparedStatement.executeQuery()
                    preparedResultSet.next()
                    assert preparedResultSet.getInt(1).intValue() == 1

                    preparedResultSet.close()
                    preparedStatement.close()
                    con.close()
                }
            }
            Thread.sleep(2000)
        }

        then:
        assert registry.get('datasource.tomcat.max').gauge().value() == 20
        assert registry.get('datasource.tomcat.idle').gauge().value() == 2
        assert registry.get('datasource.tomcat.created').gauge().value() == 38
        assert registry.get('datasource.tomcat.active').gauge().value() == 0
        assert registry.get('datasource.tomcat.waiting').gauge().value() == 0
        assert registry.get('datasource.tomcat.utilisation').gauge().value() > 0
    }
}
