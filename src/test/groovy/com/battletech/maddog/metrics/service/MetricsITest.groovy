package com.battletech.maddog.metrics.service

import com.battletech.maddog.metrics.IntegrationSpec
import com.battletech.maddog.metrics.annotation.Counted
import com.battletech.maddog.metrics.annotation.ExceptionCounted
import com.battletech.maddog.metrics.annotation.Gauged
import com.battletech.maddog.metrics.annotation.Timed
import io.micrometer.core.instrument.Measurement
import io.micrometer.core.instrument.MeterRegistry
import io.micrometer.core.instrument.Statistic
import io.micrometer.core.instrument.Timer
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import spock.lang.Subject

@Component
class MetricServiceImpl {

    @Counted(name="metricsWrapper.doService")
    def doService() {
        // nothing
    }

    @ExceptionCounted(name="metricsWrapper.doService1")
    def doService1() {
        throw new RuntimeException("Something happened!!!")
    }

    @Timed(name="metricsWrapper.doService3")
    def doService3() {
        Thread.sleep(200)
    }

    @Gauged(name="metricsWrapper.doService2")
    double doService2() {
        new Random().nextDouble()
    }
}

class MetricsITest extends IntegrationSpec {

    @Autowired
    @Subject
    MetricServiceImpl subject

    @Autowired
    MeterRegistry meterRegistry


    def "test @counted annotation"() {
        when:
        3.times {
            subject.doService()
        }

        then:
        assert meterRegistry.get("counter.metrics-wrapper.do-service").counter().count() == 3
    }

    def "test @ExceptionCounted annotation"() {
        when:
        3.times {
            try{
                subject.doService1()
            } catch (Exception e){}
        }

        then:
        assert meterRegistry.get("counter.metrics-wrapper.do-service1.exceptions").counter().count() == 3
        assert meterRegistry.find("counter.metrics-wrapper.do-service1").counter() == null
    }

    def "test @Gauge annotation"() {
        when:
        double result = subject.doService2()

        then:
        assert meterRegistry.get("gauge.metrics-wrapper.do-service2").gauge().value() == result
    }

    def "test @Timed annotation"() {
        when:
        20.times {
            subject.doService3()
        }

        then:
        Timer timer = meterRegistry.get("timer.metrics-wrapper.do-service3").timer()
        assert timer
        Iterable<Measurement> measurements = timer.measure()
        assert measurements

        Measurement count =  measurements.find{it.statistic == Statistic.COUNT} as Measurement
        assert count
        assert count.value == 20

        assert measurements.find{it.statistic == Statistic.MAX}
        assert measurements.find{it.statistic == Statistic.TOTAL_TIME}

    }
}
