package com.battletech.maddog.metrics.helper

import org.apache.tomcat.jdbc.pool.DataSource
import org.springframework.transaction.TransactionDefinition

class DbHelper {

    static DataSource getTomcatDataSource(def config = [:]) {
        new DataSource(
                url: config.dbURL,
                driverClassName: config.dbDriver?:'org.hsqldb.jdbc.JDBCDriver',
                username: config.dbUser,
                password: config.dbPwd,
                maxActive: config.maxActive?:10,
                maxAge: config.maxAge?:600000,
                maxIdle: config.maxIdle?:5,
                maxWait: config.maxWait?:10000,
                minIdle: config.minIdle?:1,
                initialSize: config.initialSize?:1,
                validationQuery: config.validationQuery?:'SELECT 1',
                testOnBorrow: config.testOnBorrow!=null?:true,
                testOnReturn: config.testOnReturn!=null?:true,
                testWhileIdle: config.testWhileIdle!=null?:true,
                defaultTransactionIsolation: TransactionDefinition.ISOLATION_READ_COMMITTED,
                minEvictableIdleTimeMillis: config.minEvictableIdleTimeMillis?:60000,
                timeBetweenEvictionRunsMillis: config.timeBetweenEvictionRunsMillis?:5000
        )
    }

    /*static HikariDataSource getHikariDataSource(def config = [:]) {
        HikariPool pool = new HikariPool(

        )

        HikariConfig hikariConfig = new HikariConfig()

        new HikariDataSource( config )
    }*/
}
