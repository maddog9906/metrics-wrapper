package com.battletech.maddog.metrics

import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import spock.lang.Specification

@SpringBootTest(classes = TestApp, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@ActiveProfiles("test")
class IntegrationSpec extends Specification {
}
