# metrics-wrapper

## What is metrics-wrapper
metrics-wrapper` is a simple wrapper that provide metrics using micrometer for monitoring of Java/Groovy application.

## Declaring the dependency
```Groovy
    repositories {
        ...
        maven { url 'https://raw.githubusercontent.com/alvin9906/maven-repo/master' }  // maven-repo at github
        ...
    }    
     
   dependencies {
        .....
        compile 'com.battletech.maddog:metrics-wrapper:0.1-spring-boot-2.1.6' 
        .....
   }
```

## Usage
You will need to import the bean configuration:

```Groovy
import com.battletech.maddog.metrics.annotation.EnableMetrics

@Configuration
@EnableMetrics
class AppConfig {
}
```

After that, you will just need to annotate the methods of your services for metrics monitoring.
The supported annotations are:
* com.battletech.maddog.metrics.annotation.Counted
* com.battletech.maddog.metrics.annotation.ExceptionCounted
* com.battletech.maddog.metrics.annotation.Timed
* com.battletech.maddog.metrics.annotation.Gauged

Example:

```Groovy

@Component
class SomeService {

    @Counted(name='doTask') //This will count the number of times `doTask` is called
    @ExceptionCounted(name='doTask') //This will count the no of times `doTask` is called and throws an exception 
    @Timed(name='doTask') //This will calculate the time taken to execute the `doTask` 
    def doTask(String args) throws Exception {
        ...
    }

}
```

